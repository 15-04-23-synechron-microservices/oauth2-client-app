package com.classpathio.oauth2.controller;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/userinfo")
@RequiredArgsConstructor
public class UserInfoRestController {
	
	private final OAuth2AuthorizedClientService oAuth2AuthorizedClientService;
	
	@GetMapping
	public Map<String, String> userInfo(OAuth2AuthenticationToken oAuth2Token){
		Map<String, String> responseMap = new LinkedHashMap<>();
		
		OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oAuth2AuthorizedClientService
			.loadAuthorizedClient(oAuth2Token.getAuthorizedClientRegistrationId(), oAuth2Token.getPrincipal().getName());
		
		if (! Objects.isNull(oAuth2AuthorizedClient)) {
			OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
			//fetch All the values and put in the hash map
			String token = accessToken.getTokenValue();
			String expiresAt = accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
			String issuedAt = accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ISO_DATE_TIME);
			Set<String> scopes = accessToken.getScopes();
			
			responseMap.put("access-token", token);
			responseMap.put("expires-at", expiresAt);
			responseMap.put("issuet-at", issuedAt);
			responseMap.put("scopes", scopes.toString());
		}
		
		return responseMap;
	}

}
