package com.classpathio.oauth2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class AppSecurityConfig {
	
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.cors().disable();
		http.csrf().disable();
		http.authorizeHttpRequests()
			.antMatchers("/login**", "/login/**", "/logout/**", "/contact-us/**")
				.permitAll()
			.anyRequest()
				.authenticated()
			.and()
				.oauth2Login()
			.redirectionEndpoint()
				.baseUri("/authorization-code/callback");
		return http.build();
	}
	
	

}
